<!DOCTYPE>
<html lang="ru">
<head>

    <title>TestApp</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="fonts/feather/iconfont.css">
    <link rel="stylesheet" href="css/ui.css">
    <link rel="stylesheet" href="js/libs/introjs/minified/introjs.min.css">
    <link rel="stylesheet" href="js/libs/toast/toastr.css">
    <link rel="stylesheet" href="js/libs/toast/toastr.style.css">

    <!-- Chart libraries -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css" integrity="sha512-/zs32ZEJh+/EO2N1b0PEdoA10JkdC3zJ8L5FTiQu82LR9S/rOQNfQN7U59U9BC12swNeRAz3HSzIL2vpp4fv3w==" crossorigin="anonymous" />

    <link rel="stylesheet" href="css/style.css">

</head>
<body>

<div class="container">
    <? include 'main' . DS . 'init.php'; ?>
</div>


<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>

<script src="js/libs/introjs/minified/intro.min.js"></script>
<script src="js/libs/toast/toastr.min.js"></script>
<script src="js/libs/toast/toastr.min.js"></script>

<!-- Chart libraries -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha512-s+xg36jbIujB2S2VKfpGmlC3T5V2TF3lY48DX7u2r9XzGzgPsa6wTpOQA7J9iffvdeBN0q9tKzRxVxw1JviZPg==" crossorigin="anonymous"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>



<script src="js/app.js"></script>

</body>
</html>