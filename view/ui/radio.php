<div class="ui-radio">
    <? foreach($variants as $value => $variant): ?>
    <? $ui_input_hash = md5(uniqid()); ?>
    <div class="ui-radio-variant">
        <input type="radio" name="<?=$name?>" id="<?=$ui_input_hash?>" value="<?=$value?>" <?=(!empty($variant['is_default']) ? 'checked' : '')?>>
        <label for="<?=$ui_input_hash?>">
            <? if(!empty($variant['icon'])): ?>
            <i class="feather <?=$variant['icon']; ?>"></i>
            <? endif; ?>
            <span><?=$variant['name']; ?></span>
        </label>
    </div>
    <? endforeach; ?>
</div>