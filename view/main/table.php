<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th></th>
        <? foreach($data->Columns as $column): ?>
        <th><?=$column?></th>
        <? endforeach; ?>
    </tr>
    </thead>
    <tbody>
    <? foreach($data->Rows as $label => $row): ?>
    <tr>
        <th><?=$label?></th>
        <? foreach($row as $value): ?>
        <td class="text-center"><?=$value?></td>
        <? endforeach; ?>
    </tr>
    <? endforeach; ?>
    </tbody>
</table>