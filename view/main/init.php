<? use Gegi\Core\UI; ?>

<div class="text-center mt-4 mb-5">
    <h1>Test application</h1>
</div>


<form method="post" enctype="multipart/form-data" id="data-form">
    <div class="row">
        <div class="col-md-3">
            <div class="card" data-step="1" data-intro="At first, choose data-source. You can load .json file or type url-address that responded with necessary data. 'Previous Data' point produced possibility for redraw saved charts.">
                <div class="card-header">
                    Choose data source
                </div>
                <div class="card-body">
                    <?= UI::radio('source', $source_options); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card" data-step="2" data-intro="After choosing data-source you will see form in this part. Load file, type URL or choose previous data variant than click Handle-button to draw charts.">
                <div class="card-header" id="js-loader-header">
                    Load JSON-file
                </div>
                <div class="card-body">

                    <div id="js-loader-form">
                        <? include TEMPLATE_DIR . 'sources' . DS . 'file.php'; ?>
                    </div>

                    <?=UI::hidden('action','handle_data'); ?>

                    <div class="mt-2">
                        <?=UI::button('Handle data','submit')?>
                    </div>


                </div>
            </div>
        </div>
        <div class="col-md-3">
            <ul class="help-links">
                <li>
                    <a href="#" id="js-save-chart" class="text-success"  data-step="6" data-intro="If you want to save chart click this link. You can to redraw saved chart by choosing it in the 'Previous data' in source choosing block.">
                        <i class="feather icon-check"></i>
                        Save chart options
                    </a>
                </li>
                <li class="mt-5">
                    <a href="#" id="js-play-user-guide">
                        <i class="feather icon-play-circle"></i>
                        Play user guide
                    </a>
                </li>
            </ul>
        </div>
    </div>
</form>


<div class="d-block d-md-flex justify-content-between">
    <div class="pills-radio" data-step="4" data-intro="Your can change chart-library by selecting one of these variants.">
        <div class="pill-header">
            Chart library
        </div>

        <?= UI::radio('chart_lib', $lib_options); ?>
    </div>

    <div class="pills-radio" data-step="5" data-intro="Your can change chart-type by selecting one of these variants.">
        <div class="pill-header">
            Chart type
        </div>

        <?= UI::radio('chart_type', $type_options); ?>
    </div>
</div>
<div class="card" data-step="3" data-intro="Your chart will appear here">
    <div class="card-body">
        <div id="charts">
            For displaying graphics choose data source and follow the instructions.
        </div>
        <div id="chart-table" class="mt-2"></div>
    </div>
</div>

