<? use Gegi\Core\UI; ?>

<? if(!empty($chart_options)): ?>
    <?= UI::radio('chart', $chart_options); ?>
<? else: ?>
    There is no saved charts.
<? endif; ?>
