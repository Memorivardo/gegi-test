<? use Gegi\Core\UI; ?>

<?=UI::text('url','Enter source url')?>

<div class="help mt-3">
    Type url address of your source.<br>
    URL must meet requirements:
    <ul>
        <li>Starts with http:// or https://</li>
        <li>Response with valid JSON data</li>
    </ul>

    Type http://207.154.251.58/?test for test
</div>
