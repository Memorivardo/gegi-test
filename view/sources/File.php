<? use Gegi\Core\UI; ?>

<?=UI::filedrop('jsonfile')?>

<div class="help mt-3">
    File must meet requirements:
    <ul>
        <li>Extension .json</li>
        <li>Size less than 2 Mb</li>
        <li>File-content - valid JSON</li>
    </ul>
</div>
