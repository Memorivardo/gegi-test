<?php

namespace Gegi\Model;

use Gegi\Core\Model;
use Gegi\Core\Tools;

class Source extends Model
{
    private $sources = [];

    function __construct()
    {
        //Getting source variants from Source-classes
        $dir = __DIR__ . DS . 'Source' . DS;
        foreach (new \DirectoryIterator($dir) as $item) {
            if (!$item->isDot() && $item->isFile()) {

                $class = str_replace('.php', '', $item->getFilename());

                $classname = 'Gegi\Model\Source\\' . $class;

                $vars = get_class_vars($classname);

                if (empty($vars['name'])) {
                    throw new \Exception('Error in class "' . $classname . '". SourceItem child class must have "name" public parameter.');
                }

                $this->sources[preg_replace('/Source/', '', $class, 1)] = [
                    'name' => $vars['name'] ?? '',
                    'icon' => $vars['icon'] ?? '',
                    'is_default' => $vars['is_default'] ?? false,
                ];
            }
        }
    }

    function getSources()
    {
        return $this->sources;
    }

    //Check if the JSON-data has right format
    static function checkData($data)
    {
        if (!is_object($data)) {
            $data = (object)$data;
        }

        if (empty($data->Columns) || empty($data->Rows)) {
            return false;
        }

        $length = count($data->Columns);

        foreach ($data->Rows as $row) {
            if (count($row) != $length) {
                return false;
            }
        }

        return true;
    }

    function getTestURLData() {
        $data = Tools::get_include_contents(UPLOADS_DIR . 'data.json');

        return json_decode($data);
    }

}