<?php

namespace Gegi\Model;

use Gegi\Core\Tools;

abstract class SourceItem
{

    private function getCurrentSource()
    {
        $classname = get_class($this);

        return str_replace('Gegi\Model\Source\Source', '', $classname);
    }

    //Get source form
    public function getForm()
    {
        $loader_path = TEMPLATE_DIR . 'sources' . DS . $this->getCurrentSource() . '.php';
        if (!file_exists($loader_path)) {
            return $this->error('Loader template didn\'t find.');
        }

        $render_vars = [];
        if (method_exists($this, 'formData')) {
            $render_vars = $this->formData();
        }

        return [
            'form' => Tools::get_include_contents($loader_path, $render_vars)
        ];
    }

    abstract function handle($type);

    protected function error($message)
    {
        return ['error' => $message];
    }
}