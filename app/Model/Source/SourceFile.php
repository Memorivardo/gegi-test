<?php

namespace Gegi\Model\Source;

use Gegi\Model\SourceItem;
use Gegi\Model\Source;

class SourceFile extends SourceItem
{

    public $icon = 'icon-upload';
    public $name = 'JSON-file';
    public $is_default = true;

    function handle($data)
    {
        $file = $data['jsonfile'];

        if (empty($file['name'])) {
            return $this->error('You forget to attach a file.');
        }

        if ($file['type'] != 'application/json') {
            return $this->error('File must have .json extension.');
        }

        $max_filesize = 2 * 1024 * 1024;
        if ($file['size'] > $max_filesize) {
            return $this->error('File must be less than 2Mb.');
        }

        $name = date('Ymd') . uniqid() . '.json';

        $fileplace = UPLOADS_DIR . $name;
        copy($file['tmp_name'], $fileplace);

        $json_string = file_get_contents($fileplace);
        $json_data = json_decode($json_string);

        if (json_last_error() != JSON_ERROR_NONE) {
            return $this->error('Content of uploaded file is not valid JSON-data.');
        }

        unlink($fileplace);

        if (!Source::checkData($json_data)) {
            return $this->error('Json-data must consists of Columns and Rows params');
        }


        return ['data' => $json_data];
    }
}