<?php

namespace Gegi\Model\Source;

use Gegi\Model\SourceItem;
use Gegi\Model\Chart;

class SourcePrevious extends SourceItem
{

    public $icon = 'icon-file-text';
    public $name = 'Previous data';

    protected function formData()
    {
        $_chart = new Chart();
        $charts = $_chart->getPrevious();

        $chart_options = [];
        foreach ($charts as $chart) {
            $chart_options[$chart->id] = [
                'name' => $chart->created . ' / ' . $chart->lib . ' / ' . $chart->type,
                'is_default' => empty($chart_options) ? true : false
            ];
        }

        return ['chart_options' => $chart_options];
    }

    function handle($data)
    {

        if (empty($data['chart']) || !is_numeric($data['chart'])) {
            $this->error('Wrong chart ID.');
        }

        $chart_model = new Chart();
        $chart = $chart_model->getById($data['chart']);
        if (empty($chart->data)) {
            $this->error('Can\'t find chart with ID ' . $data['chart']);
        }

        return [
            'data' => json_decode($chart->data),
            'lib' => $chart->lib,
            'type' => $chart->type
        ];
    }
}