<?php

namespace Gegi\Model\Source;

use Gegi\Model\SourceItem;
use Gegi\Model\Source;

class SourceUrl extends SourceItem
{

    public $icon = 'icon-globe';
    public $name = 'URL-address';

    function handle($data)
    {
        if (!filter_var($data['url'], FILTER_VALIDATE_URL)) {
            return $this->error('URL-address is incorrect');
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $_POST['url']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($curl);


        if (curl_error($curl)) {
            return $this->error('Can\'t get data from URL.');
        }

        curl_close($curl);

        $json_data = json_decode($data);
        if (json_last_error() != JSON_ERROR_NONE) {
            return $this->error('Content of uploaded file is not valid JSON-data.');
        }

        if (!Source::checkData($json_data)) {
            return $this->error('Json-data must consists of Columns and Rows params');
        }

        return ['data' => $json_data];
    }
}