<?php

namespace Gegi\Model\Chart;

use Gegi\Model\ChartItem;
use Gegi\Model\Source;

class ChartChartsjs extends ChartItem
{

    public $name = 'Charts.js';
    public $is_default = true;

}