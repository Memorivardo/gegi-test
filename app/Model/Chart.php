<?php

namespace Gegi\Model;

use Gegi\Core\Model;

class Chart extends Model
{
    protected $table = 'chart';

    private $libraries = [];

    private $types = [
        'line' => ['name' => 'Linear', 'icon' => 'icon-trending-up', 'is_default' => true],
        'bar' => ['name' => 'Bars', 'icon' => 'icon-bar-chart'],
        'radar' => ['name' => 'Radar', 'icon' => 'icon-target']
    ];

    function __construct()
    {
        //Getting chart libraries from Chart-classes
        $dir = __DIR__ . DS . 'Chart' . DS;
        foreach (new \DirectoryIterator($dir) as $item) {
            if (!$item->isDot() && $item->isFile()) {

                $class = str_replace('.php', '', $item->getFilename());

                $classname = 'Gegi\Model\Chart\\' . $class;

                $vars = get_class_vars($classname);

                if (empty($vars['name'])) {
                    throw new \Exception('Error in class "' . $classname . '". ChartItem child class must have "name" public parameter.');
                }

                $this->libraries[preg_replace('/Chart/', '', $class, 1)] = [
                    'name' => $vars['name'] ?? '',
                    'icon' => $vars['icon'] ?? '',
                    'is_default' => $vars['is_default'] ?? false,
                ];
            }
        }
    }

    function getLibraries()
    {
        return $this->libraries;
    }

    function getTypes()
    {
        return $this->types;
    }

    function getPrevious()
    {

        $charts = $this->getAll([
            'id',
            'created',
            'type',
            'lib'
        ], 'id DESC');

        return $charts;
    }

    public function save($data)
    {
        if (empty($data['chart']['data'])) {
            return $this->error('There is no data to save.');
        }

        if (empty($this->libraries[$data['chart']['lib']])) {
            $this->error('Chart library ' . $data['chart']['lib'] . ' is not allowed.');
        }

        if (empty($this->types[$data['chart']['type']])) {
            $this->error('Chart type ' . $data['chart']['type'] . ' is not allowed.');
        }

        if (!Source::checkData($data['chart']['data'])) {
            $this->error('Chart data is not valid.');
        }

        $info = json_encode($data['chart']['data'], JSON_UNESCAPED_UNICODE);

        $chart_info = [
            'type' => $data['chart']['type'],
            'lib' => $data['chart']['lib'],
            'data' => $info
        ];

        //Create checksum to avoid saving existing row many times
        $checksum = md5(json_encode($chart_info));
        $chart_info['checksum'] = $checksum;
        $chart_info['created'] = date('Y-m-d H:i:s');


        if ($this->getByChecksum($checksum)) {
            return $this->error('Chart options are already saved');
        }

        $this->add($chart_info);

        return $this->success('Chart options has been saved. Now you can retrieve it by choosing Previous Data option.');
    }

    function getByChecksum($checksum)
    {

        $this->info = $this->get(false, [
            'checksum' => $checksum
        ]);

        return $this->info ?? false;
    }

}
