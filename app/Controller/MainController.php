<?php


namespace Gegi\Controller;

use Gegi\Core\Controller;
use Gegi\Model\Source;
use Gegi\Model\Chart;
use Gegi\Core\Tools;

class MainController extends Controller
{
    private $_chart;
    private $_source;

    function __construct()
    {
        $this->_source = new Source();
        $this->_chart = new Chart();

        parent::__construct();
    }

    function init()
    {

        if (isset($_GET['test'])) {
            $this->response($this->_source->getTestURLData());
        }
        $this->view['source_options'] = $this->_source->getSources();
        $this->view['lib_options'] = $this->_chart->getLibraries();
        $this->view['type_options'] = $this->_chart->getTypes();

    }

    function get_source_field()
    {
        if (empty($_POST['source'])) {
            $this->error('Missing source param');
        }

        try {
            $source_class = 'Gegi\Model\Source\Source' . $_POST['source'];
            $source = new $source_class();

            $this->response($source->getForm());
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

    }


    function handle_data()
    {
        if (empty($_POST['source'])) {
            $this->error('Missing source param');
        }

        try {
            $source_class = 'Gegi\Model\Source\Source' . $_POST['source'];
            $source = new $source_class();

            $this->response($source->handle($_POST + $_FILES));
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }


    function save_chart()
    {
        $this->response($this->_chart->save($_POST));
    }

    function get_table()
    {
        if (!Source::checkData($_POST['chart']['data'])) {
            $this->error('Chart data is not valid.');
        }

        $this->response([
            'html' => Tools::get_include_contents(TEMPLATE_DIR . 'main' . DS . 'table.php', [
                'data' => (object)$_POST['chart']['data']
            ])
        ]);
    }
}