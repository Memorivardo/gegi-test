<?php

namespace Gegi\Core;


class Core
{

    static function init()
    {
        try {

            DB::connect();

            $route = 'main';

            $controller_name = 'Gegi\\Controller\\' . ucfirst($route) . 'Controller';
            $controller = new $controller_name();
            $controller->render();

        } catch (\Exception $e) {
            die($e->getMessage());
        } catch (\TypeError $e) {
            die($e->getMessage());
        }
    }
}