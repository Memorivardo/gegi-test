<?php

namespace Gegi\Core;

class Model
{
    /**
     * @param $data
     * @return mixed
     */
    function add($data)
    {

        $insert_query = 'INSERT INTO ' . $this->table . '(' . implode(',',
                array_keys($data)) . ') VALUES (' . implode(',', array_map(function () {
                return '?';
            }, $data)) . ')';

        return DB::insert($insert_query, array_values($data));
    }

    /**
     * @param $fields
     * @param array $conditions
     * @param string $order
     * @param bool $multiple
     * @return mixed
     */
    function get($fields, array $conditions = [], $order = '', $multiple = false)
    {
        $select_query = 'SELECT ' . (empty($fields) ? '*' : implode(',', $fields));
        $select_query .= ' FROM `' . $this->table . '`';

        if (!empty($conditions)) {
            $select_query .= ' WHERE ' . implode(' AND ', array_map(function ($field) {
                    return '`' . $field . '` = ?';
                }, array_keys($conditions)));
        }

        if ($order) {
            $select_query .= ' ORDER BY ' . $order;
        }

        $result = DB::select($select_query, array_values($conditions), $multiple);

        return $result;
    }

    /**
     * @param bool $fields
     * @param string $order
     * @return mixed
     */
    function getAll($fields = false, $order = '')
    {
        return $this->get($fields, [], $order, true);
    }

    /**
     * @param $id
     * @return mixed
     */
    function getById($id)
    {
        return $this->get(false, ['id' => $id]);
    }

    function error($message)
    {
        return ['error' => $message];
    }

    function success($message)
    {
        return ['success' => $message];
    }
}