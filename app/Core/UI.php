<?php

namespace Gegi\Core;

use Gegi\Core\Tools;

class UI
{

    static $templates;

    /**
     * @param string $template_name
     * @param array $variables
     * @return bool|false|string
     */
    static function render(string $template_name, $variables = [])
    {
        $template = TEMPLATE_DIR . 'ui' . DS . $template_name . '.php';

        $variables['ui_input_hash'] = md5(uniqid());

        return Tools::get_include_contents($template, $variables);

    }


    /**
     * @param $name
     * @param $variants - array where keys are input values and elements are arrays like ['name' => '', 'icon' => '', 'is_default' =>]
     * name is required
     * icon is optional, default - false. This is code from feather-icon-set
     * is_default is optional, default - false. This is a flag for checking that this value is default for this radio-set
     * @return false|string width HTML of chosen template
     * @throws Exception
     */
    static function radio(string $name, array $variants)
    {
        return self::render('radio', [
            'name' => $name,
            'variants' => $variants
        ]);
    }

    /**
     * @param string $name
     * @return bool|false|string
     */
    static function filedrop(string $name)
    {

        return self::render('filedrop', [
            'name' => $name
        ]);
    }

    /**
     * @param string $value
     * @param string $type
     * @return bool|false|string
     */
    static function button(string $value, $type = 'button')
    {

        return self::render('button', [
            'type' => $type,
            'value' => $value
        ]);
    }

    /**
     * @param string $name
     * @param $placeholder
     * @return bool|false|string
     */
    static function text(string $name, $placeholder)
    {
        return self::render('text', [
            'name' => $name,
            'placeholder' => $placeholder
        ]);
    }

    /**
     * @param string $name
     * @param string $value
     * @return bool|false|string
     */
    static function hidden(string $name, string $value)
    {
        return self::render('hidden', [
            'name' => $name,
            'value' => $value
        ]);
    }
}