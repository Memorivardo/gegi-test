<?php

namespace Gegi\Core;

class Controller
{

    protected $template;
    protected $view = [];

    /**
     * Controller constructor.
     * @throws Exception
     */
    function __construct()
    {
        if (!empty($_POST['action'])) {
            if (method_exists($this, $_POST['action'])) {
                $this->{$_POST['action']}();
            } else {
                throw new \Exception('Не найден обработчик для действия ' . $_POST['action']);
            }
        }

    }

    /**
     * @throws Exception
     */
    function render()
    {
        //If we need to execute some operations before render
        if (method_exists($this, 'init')) {
            $this->init();
        }

        extract($this->view, EXTR_SKIP);

        $template = !empty($this->template) ? str_replace('.', DIRECTORY_SEPARATOR, $this->template) : 'layout';

        ob_start();

        $filename = TEMPLATE_DIR . $template . '.php';
        if (file_exists($filename)) {
            include $filename;
        } else {
            throw new \Exception('Can\'t find template ' . $template);
        }

        die(ob_get_clean());
    }

    /**
     * @param $message
     * @param array $data
     */
    function error($message, $data = [])
    {
        $this->response([
                'error' => $message
            ] + $data);
    }

    /**
     * @param $message
     * @param array $data
     */
    function success($message, $data = [])
    {
        $this->response([
                'success' => $message
            ] + $data);
    }

    /**
     * @param $data
     */
    function response($data)
    {
        header('Content-Type: application/json');
        die(json_encode($data));
    }
}