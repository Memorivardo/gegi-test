let App = {
    loader: 'file',
    chart_info: {
        data: false,
        lib: false,
        type: false
    },
    init: function () {

        App.chart_info.type = $('[name="chart_type"]:checked').val();
        App.chart_info.lib = $('[name="chart_lib"]:checked').val();

        $('[name="chart_type"]').change(function () {
            App.chart_info.type = $(this).val();
            App.scrollTo('#charts');
            App.render();
        });

        $('[name="chart_lib"]').change(function () {
            App.chart_info.lib = $(this).val();
            App.scrollTo('#charts');
            App.render();
        });


        $('[name="source"]').change(function () {

            let source = $(this).val();
            let label = $(this).next('label').find('span').html();

            //Get source-fields template
            App.ajax({
                action: 'get_source_field',
                source: source
            }, function (msg) {
                App.toast(msg);

                let loader_block = $('#js-loader-form');
                if (msg.error)
                    loader_block.html('');
                else
                    loader_block.html(msg.form);

                $('#js-loader-header').html(label);

                App.scrollTo('#js-loader-header');
            });
        });
        $('[name="source"]').change();

        $('#data-form').submit(function () {

            let formData = new FormData(this);

            //Provide source-data
            App.ajax(formData, function (msg) {
                App.toast(msg);

                if (msg.error)
                    return false;

                //change chart-library if it is provided
                if (msg.lib) {
                    $('[name="chart_lib"][value="' + msg.lib + '"]').prop('checked', true);
                    App.chart_info.lib = msg.lib;
                }

                //change chart-type if it is provided
                if (msg.type) {
                    $('[name="chart_type"][value="' + msg.type + '"]').prop('checked', true);
                    App.chart_info.type = msg.type;
                }

                //update chart data
                App.chart_info.data = msg.data;
                App.render();

                App.scrollTo('#charts');


            }, true);

            return false;
        });


        $('#js-play-user-guide').click(function () {

            introJs().start(); //Start Intro tour

            return false;
        });

        $('#js-save-chart').click(function () {
            if (!App.chart_info.data) {
                App.toast({error: 'There is no data to save.'});
                return false;
            }

            App.ajax({
                action: 'save_chart',
                chart: App.chart_info
            }, function (msg) {
                App.toast(msg);

                if (msg.success && $('[name="source"]:checked').val() == 'previous') {
                    $('[name="source"]').change();
                }

            });

            return false;
        });
    },
    scrollTo: function (selector) {

        $('html, body').animate({
            scrollTop: $(selector).offset().top
        }, 1000);
    },
    render: function () {

        if (!App.chart_info.data)
            return false;

        let charts_block = $('#charts');

        if (typeof App.chart[App.chart_info.lib] == 'undefined') {
            App.toast({error: 'Can\'t find chart library "' + App.chart_info.lib + '".'});
            return false;
        } else {
            //Call library render function
            App.chart[App.chart_info.lib].render(charts_block, App.chart_info.data.Columns, App.chart_info.data.Rows, App.chart_info.type);

            charts_block.css('min-height', charts_block.find('.chart-place').height());

            App.render_table();
        }

    },
    render_table: function(){
        $table_block = $('#chart-table');

        App.ajax({
            action: 'get_table',
            chart: App.chart_info
        }, function (msg) {

            $table_block.html(msg.html)

        });
    },
    chart: {
        Chartsjs: {
            render: function ($charts_block, labels, rows, type) {

                $charts_block.html('<canvas class="chart-place"></canvas>');

                let datarows = [];

                for (let i in rows) {

                    let row = rows[i];

                    let color = App.getRandColor();

                    datarows.push({
                        label: i,
                        borderColor: color,
                        backgroundColor: color,
                        fill: false,
                        data: row,

                    });
                }

                new Chart($charts_block.find('canvas'), {
                    type: type, //bar | line
                    data: {
                        labels: labels,
                        datasets: datarows
                    },
                    responsive: true,
                    hoverMode: 'index',
                    stacked: false,
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
            }
        },
        Highcharts: {
            render: function ($charts_block, labels, rows, type) {
                $charts_block.html('<div id="highchart" class="chart-place"></div>');

                let series = [];
                let series_type = '';
                switch (type) {
                    case 'line':
                        series_type = 'line';
                        break;

                    case 'bar':
                        series_type = 'column';
                        break;

                    case 'radar':
                        series_type = 'area';
                        break;
                }


                for (let i in rows) {

                    series.push({
                        name: i,
                        data: rows[i].map(function (i) {
                            return parseInt(i);
                        }),
                        type: series_type
                    });
                }

                let chart_options = {
                    xAxis: {
                        categories: labels
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    series: series
                };

                if (type == 'radar') {
                    chart_options.chart = {
                        polar: true
                    };

                    chart_options.pane = {
                        startAngle: 0,
                        endAngle: 360
                    };
                }
                Highcharts.chart('highchart', chart_options);

            }
        },
        Apexcharts: {

            render: function ($charts_block, labels, rows, type) {
                $charts_block.html('<div id="apexchart" class="chart-place"></div>');

                let series = [];

                for (let i in rows) {
                    series.push({
                        name: i,
                        data: rows[i]
                    });

                }

                let options = {
                    chart: {
                        type: type
                    },
                    stroke: {
                        curve: 'smooth',
                    },
                    series: series,
                    xaxis: {
                        categories: labels
                    }
                };

                let chart = new ApexCharts(document.querySelector("#apexchart"), options);

                chart.render();
            }
        }
    },
    getRandColor: function () {
        let r = Math.floor(Math.random() * 255);
        let g = Math.floor(Math.random() * 255);
        let b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    },
    ajax: function (data, callback = function () {
    }, hasFile = false) {

        try {
            let ajax_data = {
                url: '?',
                type: 'POST',
                dataType: 'json',
                data: data,
                cache: false,
                success: callback
            };

            if (hasFile) {
                ajax_data.contentType = false;
                ajax_data.processData = false;
            }

            $.ajax(ajax_data);
        } catch (e) {

        }
    },
    toast: function (message) {
        if (message.success) {
            toastr.success(message.success, 'Success!', {
                positionClass: 'toast-bottom-right',
                containerId: 'toast-bottom-right'
            });
        } else if (message.error) {
            toastr.error(message.error, 'Error!', {
                positionClass: 'toast-bottom-right',
                containerId: 'toast-bottom-right'
            });
        }
    }
};

App.init();